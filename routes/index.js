const express = require('express');
const router = express.Router();

//importar express validator
const { body } = require('express-validator/check')
const proyectosController = require('../controller/proyectosController');

module.exports = () => {
	// Ruta para la raiz
	router.get('/', proyectosController.proyectosHome);

	router.get('/nuevo-proyecto', proyectosController.formularioProyecto);
	router.post('/nuevo-proyecto', body('nombre').not().isEmpty().trim().escape(), proyectosController.nuevoProyecto);
	// Listar el proyecto
	router.get('/proyectos/:url', proyectosController.proyectoPorURL);

	// Actualizar el proyecto
	router.get('/proyecto/editar/:id', proyectosController.formularioEditar);
	router.post('/nuevo-proyecto/:id', body('nombre').not().isEmpty().trim().escape(), proyectosController.actualizarProyecto);

	//Eliminar Eliminar proyecto
	router.delete('/proyectos/:url', proyectosController.eliminarProyecto);
	
	return router;
};
