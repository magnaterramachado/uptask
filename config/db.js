const {Sequelize,QueryTypes} = require('sequelize');

const db = new Sequelize('uptasknode', 'prueba', '123456', {
  host: 'localhost',
  dialect: 'postgres',
  port: '5432',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

//getLastId = () => db.query("SELECT last_value FROM proyecto_id_seq;").then( res =>res[0][0].last_value);

//getLastId = () => db.query("SELECT last_value AS val FROM proyecto_id_seq;", { type: QueryTypes.SELECT })
//  .then(async res => res[0].val);

//module.exports = {db, getLastId};

module.exports = {db};
