//const {db,getLastId} = require('../config/db');
const {db} = require('../config/db');
const Sequelize = require('sequelize');

const Proyecto = db.define('proyecto', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  nombre: Sequelize.STRING(60),
  url: Sequelize.STRING(60)
},
{
  tableName:'proyecto',
  hooks: {
    async beforeCreate(proyecto) {
      const nombre = proyecto.nombre.replace(' ','-').toLowerCase();
      //const cant_proyectos = (await db.query(`SELECT * FROM proyecto WHERE LOWER(nombre)='${proyecto.nombre.toLowerCase()}'`))[1].rowCount;
      const cant_proyectos = (await db.query(`SELECT * FROM proyecto WHERE LOWER(url) ~ '${nombre}'`))[1].rowCount;

      proyecto.url =  cant_proyectos > 0 ? nombre + '-' + cant_proyectos : nombre;

    }
  }
});

module.exports = Proyecto;
