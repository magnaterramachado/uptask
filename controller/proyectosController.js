const Proyecto = require('../model/Proyecto');

exports.proyectosHome = async (req,res) => {
const proyectos = await Proyecto.findAll();

	res.render('index', {
		nombrePagina: 'Proyectos',
		proyectos
	});
}

exports.formularioProyecto = async (req,res) => {
	const proyectos = await Proyecto.findAll();
	res.render('nuevoProyecto', {
		nombrePagina: 'Nuevo Proyecto',
		proyectos
	});
}

exports.nuevoProyecto = async (req,res) => {
	const proyectos = await Proyecto.findAll();
	//Enviar a consola lo que el usuario escribió
	//console.log(req.body);

	//Validar que input no sea vacío
	const {nombre} = req.body;

	let errores = [];

	if (!nombre)
		errores.push({'texto': 'Agrega un Nombre al Proyecto'});

	if (errores.length > 0)
		res.render('nuevoProyecto', {
			nombrePagina: 'Nuevo Proyecto',
			errores,
			proyectos
		});
	else {
		await Proyecto.create({nombre});
		res.redirect('/');
	}
}

exports.proyectoPorURL = async (req, res, next) => {
	const proyectosPromise =  Proyecto.findAll();
	const proyectoPromise =  Proyecto.findOne({
		where: {
			url: req.params.url
		}
	});

	const [proyecto, proyectos] = await Promise.all([proyectoPromise,proyectosPromise]);

	if (!proyecto) return next();

	res.render('tareas', {
		nombrePagina: 'Tareas del proyecto',
		proyecto,
		proyectos
	})
}

exports.formularioEditar = async (req,res) => {
	const proyectosPromise =  Proyecto.findAll();
	const proyectoPromise =  Proyecto.findOne({
		where: {
			id: req.params.id
		}
	});

	const [proyecto, proyectos] = await Promise.all([proyectoPromise,proyectosPromise]);

	//render a la vista
	res.render('nuevoProyecto', {
		nombrePagina: 'Editar Proyecto',
		proyectos,
		proyecto
	})
}

exports.actualizarProyecto = async (req,res) => {
	const proyectosPromise =  Proyecto.findAll();
	const proyectoPromise =  Proyecto.findOne({
		where: {
			id: req.params.id
		}
	});

	const [proyecto, proyectos] = await Promise.all([proyectoPromise,proyectosPromise]);

	//Validar que input no sea vacío
	const {nombre} = req.body;

	let errores = [];

	if (!nombre)
		errores.push({'texto': 'Agrega un Nombre al Proyecto'});

	if (errores.length > 0)
		res.render('nuevoProyecto', {
			nombrePagina: 'Editar Proyecto',
			errores,
			proyectos
		});
	else {
		proyecto.nombre = nombre;
		await proyecto.save();
		// await Proyecto.update(
		// 	{ nombre},
		// 	{ where : { id : req.params.id} }
		// );
		res.redirect('/');
	}
}

exports.eliminarProyecto = async (req, res, next) => {
	//req obtiene la información, se puede usar .query o .params para leer los datos enviados
	const {urlProyecto} = req.query;

	const resultado = await Proyecto.destroy({
		where: {
			url: urlProyecto
		}
	});

	if (!resultado) return next();

	res.status(200).send('Su proyecto fue eliminado.');
}
