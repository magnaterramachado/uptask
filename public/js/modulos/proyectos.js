import Swal from 'sweetalert2';
import axios from 'axios';

const btnEliminar = document.querySelector('#eliminar-proyecto');

if (btnEliminar) {
  btnEliminar.addEventListener('click', e => {
    const urlProyecto = e.target.dataset.proyectoUrl;

    Swal.fire({
      title: '¿Está seguro que desea eliminar el proyecto?',
      text: "Si acepta el registro no podrá recuperarse",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar'
    })
      .then( res => {
        if (res.isConfirmed) {
          //enviar petición a axios
          const url = `${location.origin}/proyectos/urlProyecto`;

          axios.delete(url, {params: {urlProyecto} })
            .then( res => {
              Swal.fire('Eliminado!', res.data, 'success');

              //Redireccionar al inicio
              setTimeout(()=>{
                window.location.href = '/'
              },3000)
            })
            .catch(() => {
              Swal.fire('Error al eliminar!', 'No se pudo eliminar.', 'error');
            });
        }
      })

  });
}

export default btnEliminar;
