const express = require('express');
const routes = require('./routes');
const path = require('path');
const bodyParser = require('body-parser');
const {db} = require('./config/db');
const helpers = require('./helpers');

//crear la conexión a la BBDD
//Si usamos authenticate no sincroniza los modelos con la BBDD
//db.authenticate()
//  .then(() => console.log('Conectado al servidor'))
//  .catch(err => console.log(err));

//Si usamos sync autenticamos y sincronizamos los MODELOS IMPORTADOS con la BBDD
//Importar el modelo
require('./model/Proyecto');
db.sync()
  .then(() => console.log('Conectado al servidor'))
  .catch(err => console.log(err));

//crear una app con express
const app = express();

//Donde encontrar los archivos estaticos
app.use(express.static('public'));

//habilitar pug
app.set('view engine','pug');

//habilitar carpate de vistas view
app.set('views',path.join(__dirname,'./view'));

//Pasar vardumps a la aplicación
app.use((req ,res, next) => {
  res.locals.vardump = helpers.vardump;

  //Next envía el control al siguiente app.use()
  next();
})

//Habilitar bodyParser para leer datos del formulario
app.use(bodyParser.urlencoded({extended: true}));

//ruta para el home
app.use('/',routes());

app.listen(3000);
